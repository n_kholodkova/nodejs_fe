import React, { useState, useContext } from 'react';
import { Route, Redirect } from 'react-router';
import Login from './Login';
import Signup from './Signup';
import Profile from './Profile';
import Header from './Header';
import ChangePassword from './ChangePassword';
import Aside from './Aside';
import Loads from './Loads';
import LoadFull from './LoadFull';
import FormLoad from './FormLoad';
import FormTruck from './FormTruck';
import Trucks from './Trucks';
import TruckFull from './TruckFull';
import { AuthContext } from '../context/context';

const App = () => {
  const [status, setStatus] = useState('NEW');
  const { user } = useContext(AuthContext);
  return (
    <>
      <Header />
      <Aside setStatus={setStatus} />
      {user ? (
        <>
          <main className="main">
            <Route path="/signin" render={() => <Redirect to='/me' />} />
            <Route path="/signup" render={() => <Redirect to='/me' />} />
            <Route exact path="/me" render={() => <Profile />} />
            <Route exact path="/" render={() => <Profile />} />
            <Route
              path="/me/changePassword"
              render={() => <ChangePassword />}
            />
            <Route
              exact
              path="/loads/list"
              render={() => <Loads status={status} setStatus={setStatus} />}
            />
            <Route exact path="/loads/:id/view" render={() => <LoadFull />} />
            <Route exact path="/loads/:id/edit" render={() => <FormLoad />} />
            <Route exact path="/loads/new/create" render={() => <FormLoad setStatus={setStatus} />} />
            <Route
              exact
              path="/loads/all/:searchstatus"
              render={() => <Loads status={status} setStatus={setStatus} />}
            />
            <Route exact path="/trucks/:id/view" render={() => <TruckFull />} />
            <Route exact path="/trucks/:id/edit" render={() => <FormTruck />} />
            <Route
              exact
              path="/trucks/new/create"
              render={() => <FormTruck />}
            />
            <Route exact path="/trucks" render={() => <Trucks />} />
          </main>
        </>
      ) : (
        <main className="main">
          <Route path="/signin" render={() => <Login />} />
          <Route path="/signup" render={() => <Signup />} />
          <Route exact path="/" render={() => <Profile />} />
        </main>
      )}
    </>
  );
};

export default App;
