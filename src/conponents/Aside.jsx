import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { AuthContext } from '../context/context';

const Aside = ({ setStatus }) => {
  const { user } = useContext(AuthContext);
  const role = user?.role;
  return (
    <aside className="aside">
      <nav>
        <ul className="navList navListAside">
          <li>
            <NavLink
              to="/me"
              className="navLink navLinkAside"
              activeClassName="navLinkAsideActive"
            >
              Profile
            </NavLink>
          </li>
          {role === 'SHIPPER' && (
            <li>
              <NavLink
                exact
                to="/loads/all/new"
                className="navLink navLinkAside"
                activeClassName="navLinkAsideActive"
                onClick={() => {
                  setStatus('NEW');
                }}
              >
                New Loads
              </NavLink>
            </li>
          )}
          <li>
            <NavLink
              exact={true}
              to="/loads/all/shipped"
              className="navLink navLinkAside"
              activeClassName="navLinkAsideActive"
              onClick={() => {
                setStatus('SHIPPED');
              }}
            >
              Shipped Loads
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/loads/all/assigned"
              className="navLink navLinkAside"
              activeClassName="navLinkAsideActive"
              onClick={() => {
                setStatus('ASSIGNED');
              }}
            >
              Assigned Loads
            </NavLink>
          </li>
          <li>
            <NavLink
              exact={true}
              to="/loads/list"
              className="navLink navLinkAside"
              activeClassName="navLinkAsideActive"
              onClick={() => {
                setStatus('');
              }}
            >
              All loads
            </NavLink>
          </li>
          {role === 'DRIVER' && (
            <li>
              <NavLink
                to="/trucks"
                className="navLink navLinkAside"
                activeClassName="navLinkAsideActive"
              >
                Trucks
              </NavLink>
            </li>
          )}
        </ul>
      </nav>
    </aside>
  );
};

export default Aside;
