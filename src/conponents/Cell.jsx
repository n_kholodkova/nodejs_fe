import React from 'react';

const Cell = ({ title, value }) => (
  <div className="cell">
    <h2 className="cellTitle">{title}</h2>
    <p className="cellText">{value}</p>
  </div>
);

export default Cell;
