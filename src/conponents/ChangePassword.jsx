import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../context/context';
import Modal from './Modal';
import Preloader from './Preloader';
import FetchData from '../helpers/dataFetcher';

const ChangePassword = () => {
  const { jwt } = useContext(AuthContext);
  const hist = useHistory();
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [error, setError] = useState(null);
  const [load, setLoad] = useState(false);
  const [success, setSuccess] = useState(null);
  const fetcher = new FetchData();
  const handler = async ({ target }) => {
    fetcher.processData({
      jwt,
      setLoad,
      setData: setSuccess,
      setError,
      type: 'CHANGE_PASSWORD',
      formData: Object.fromEntries([...new FormData(target)]),
    });
  };
  return (
    <>
      {jwt && (
        <section className="section">
          <form
            className="form"
            onSubmit={(event) => {
              event.preventDefault();
              handler(event);
            }}
          >
            <label className="label">
              <input
                className="input"
                type="password"
                placeholder="Old password"
                onChange={({ target }) => {
                  setOldPassword(target.value);
                }}
                value={oldPassword}
                name="oldPassword"
                required
              />
            </label>
            <label className="label">
              <input
                className="input"
                type="password"
                placeholder="New password"
                onChange={({ target }) => {
                  setNewPassword(target.value);
                }}
                value={newPassword}
                name="newPassword"
                required
              />
            </label>
            <button type="submit" className="btn btnSubmit">
              Submit
            </button>
          </form>
        </section>
      )}
      {error && <Modal message={error} setMessage={setError} isError />}
      {success && (
        <Modal
          message={success.message}
          setMessage={setSuccess}
          successAction={() => hist.push('/me')}
        />
      )}
      {load && <Preloader />}
    </>
  );
};

export default ChangePassword;
