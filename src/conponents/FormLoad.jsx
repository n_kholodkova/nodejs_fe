import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { AuthContext } from '../context/context';
import FetchData from '../helpers/dataFetcher';
import Modal from './Modal';
import Preloader from './Preloader';

const FormLoad = ({setStatus}) => {
  const { id } = useParams();
  const [data, setData] = useState(null);
  const [isLoad, setIsLoad] = useState(false);
  const hist = useHistory();
  const { load } = data ?? {};
  const { jwt } = useContext(AuthContext);
  const [name, setName] = useState(load?.name ?? '');
  const [payload, setPayload] = useState(load?.payload ?? 0);
  const [pickupAddress, setPickupAddress] = useState(
    load?.pickup_address ?? ''
  );
  const [deliveryAddress, setDeliveryAddress] = useState(
    load?.delivery_address ?? ''
  );
  const [width, setWidth] = useState(load?.dimensions?.width ?? 0);
  const [length, setLength] = useState(load?.dimensions?.length ?? 0);
  const [height, setHeight] = useState(load?.dimensions?.height ?? 0);
  const [success, setSuccess] = useState(null);
  const [error, setError] = useState(null);
  const fetcher = new FetchData();
  useEffect(() => {
    if (id) {
      fetcher.processData({
        jwt,
        setData: setData,
        setLoad: setIsLoad,
        setError,
        type: 'GET_LOAD',
        id,
      });
    }
  }, []);
  useEffect(() => {
    setName(load?.name ?? '');
    setPayload(load?.payload ?? '');
    setPickupAddress(load?.pickup_address ?? '');
    setDeliveryAddress(load?.delivery_address ?? '');
    setWidth(load?.dimensions?.width ?? '');
    setLength(load?.dimensions?.length ?? '');
    setHeight(load?.dimensions?.height ?? '');
  }, [load?._id]);

  const handler = async ({ target }) => {
    const formData = new FormData(target);
    const sendData = {
      name: formData.get('name'),
      payload: formData.get('payload'),
      pickup_address: formData.get('pickup_address'),
      delivery_address: formData.get('delivery_address'),
      dimensions: {
        width: formData.get('width'),
        length: formData.get('length'),
        height: formData.get('height'),
      },
    };
    let type;
    if (id) {
      type = 'UPDATE_LOAD';
    } else {
      type = 'ADD_LOAD';
    }
    fetcher.processData({
      jwt,
      setData: setSuccess,
      setLoad: setIsLoad,
      setError,
      type: type,
      id,
      formData: sendData,
    });
  };

  return (
    <>
      <section className="section">
        <form
          onSubmit={(event) => {
            event.preventDefault();
            handler(event);
          }}
          className="form"
        >
          <label className="label">
            <span className="labelSpan">Name</span>
            <input
              onChange={({ target }) => {
                setName(target.value);
              }}
              type="text"
              name="name"
              value={name}
              className="input"
              placeholder="Name"
            />
          </label>
          <label className="label">
            <span className="labelSpan">Pickup address</span>
            <input
              onChange={({ target }) => {
                setPickupAddress(target.value);
              }}
              type="text"
              name="pickup_address"
              placeholder="Pickup address"
              value={pickupAddress}
              className="input"
            />
          </label>
          <label className="label">
            <span className="labelSpan">Delivery address</span>
            <input
              onChange={({ target }) => {
                setDeliveryAddress(target.value);
              }}
              placeholder="Delivery address"
              type="text"
              name="delivery_address"
              value={deliveryAddress}
              className="input"
            />
          </label>
          <label className="label">
            <span className="labelSpan">Payload</span>
            <input
              onChange={({ target }) => {
                setPayload(target.value);
              }}
              type="number"
              min="0"
              step="1"
              name="payload"
              placeholder="Payload"
              value={payload}
              className="input"
            />
          </label>
          <label className="label">
            <span className="labelSpan">Width</span>
            <input
              className="input"
              onChange={({ target }) => {
                setWidth(target.value);
              }}
              placeholder="Width"
              type="number"
              min="0"
              step="1"
              max="350"
              name="width"
              value={width}
            />
          </label>
          <label className="label">
            <span className="labelSpan">Length</span>
            <input
              className="input"
              onChange={({ target }) => {
                setLength(target.value);
              }}
              placeholder="Width"
              type="number"
              min="0"
              step="1"
              max="200"
              name="length"
              value={length}
            />
          </label>
          <label className="label">
            <span className="labelSpan">Height</span>
            <input
              className="input"
              placeholder="Width"
              onChange={({ target }) => {
                setHeight(target.value);
              }}
              type="number"
              min="0"
              step="1"
              max="700"
              name="height"
              value={height}
            />
          </label>
          <button className="btn btnSubmit" type="submit">
            Submit
          </button>
        </form>
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {success && <Modal message={success.message} setMessage={setSuccess} successAction={() => {
        if (!load?._id) {
          setStatus('NEW')
        }
        hist.push(`/loads${load?._id ? `/${load._id}/view` : '/list'}`)
      }} />}
      {isLoad && <Preloader />}
    </>
  );
};

export default FormLoad;
