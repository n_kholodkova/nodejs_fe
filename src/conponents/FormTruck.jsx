import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { AuthContext } from '../context/context';
import FetchData from '../helpers/dataFetcher';
import Modal from './Modal';
import Preloader from './Preloader';

const FormTruck = () => {
  const { id } = useParams();
  const [truck, setTruck] = useState(null);
  const [isLoad, setIsLoad] = useState(false);
  const hist = useHistory();
  const { jwt } = useContext(AuthContext);
  const [type, setType] = useState(truck?.type ?? '');
  const [success, setSuccess] = useState(null);
  const [error, setError] = useState(null);
  const fetcher = new FetchData();
  useEffect(() => {
    if (id) {
      fetcher.processData({
        jwt,
        setData: setTruck,
        setLoad: setIsLoad,
        setError,
        type: 'GET_TRUCK',
        id,
      });
    }
  }, []);
  useEffect(() => {
    setType(truck?.type ?? '');
  }, [truck?._id]);

  const handler = async ({ target }) => {
    const sendData = Object.fromEntries([...new FormData(target)]);
    let type;
    if (id) {
      type = 'UPDATE_TRUCK';
    } else {
      type = 'ADD_TRUCK';
    }
    fetcher.processData({
      jwt,
      setData: setSuccess,
      setLoad: setIsLoad,
      setError,
      type: type,
      id,
      formData: sendData,
    });
  };

  return (
    <>
      <section className="section">
        <form
          onSubmit={(event) => {
            event.preventDefault();
            handler(event);
          }}
          className="form"
        >
          <label className="label">
            <span className="labelSpan">Name</span>
            <input
              onChange={({ target }) => {
                setType(target.value);
              }}
              type="text"
              name="type"
              value={type}
              className="input"
              placeholder="Type"
            />
          </label>
          <button className="btn btnSubmit" type="submit">
            Submit
          </button>
        </form>
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {success && <Modal message={success.message} setMessage={setSuccess} successAction={() => hist.push('/trucks')} />}
      {isLoad && <Preloader />}
    </>
  );
};

export default FormTruck;
