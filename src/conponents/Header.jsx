import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { AuthContext } from '../context/context';

const Header = () => {
  const { user, setUser, setJwt } = useContext(AuthContext);
  return (
    <header className="header">
      <nav>
        <ul className="navList navListHeader">
          {!user && (
            <li className="navItemHeader">
              <NavLink
                to="/signin"
                className="navLink navLinkHeader"
                activeClassName="navLinkHeaderActive"
              >
                Login
              </NavLink>
            </li>
          )}
          {!user && (
            <li className="navItemHeader">
              <NavLink
                to="/signup"
                className="navLink navLinkHeader"
                activeClassName="navLinkActive"
              >
                Signup
              </NavLink>
            </li>
          )}
          {user?.role === 'SHIPPER' && (
            <li className="navItemHeader">
              <NavLink
                to="/loads/new/create"
                className="navLink navLinkHeader"
                activeClassName="navLinkHeaderActive"
              >
                New load
              </NavLink>
            </li>
          )}
          {user?.role === 'DRIVER' && (
            <li className="navItemHeader">
              <NavLink
                to="/trucks/new/create"
                className="navLink navLinkHeader"
                activeClassName="navLinkHeaderActive"
              >
                New truck
              </NavLink>
            </li>
          )}
          {user && (
            <li className="navItemHeader">
              <NavLink
                to="/logout"
                className="navLink navLinkHeader"
                activeClassName="navLinkHeaderActive"
                onClick={(event) => {
                  event.preventDefault();
                  setJwt('');
                  setUser(null);
                }}
              >
                Logout
              </NavLink>
            </li>
          )}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
