import React, { useState } from 'react';
import { Link, useHistory, Redirect } from 'react-router-dom';
import Eye from './Eye';
import Edit from './Edit';
import Cell from './Cell';
import FetchData from '../helpers/dataFetcher';
import Modal from './Modal';
import Preloader from './Preloader';

const Load = ({ load, jwt, user, setStatus, setShouldUpdate }) => {
  const hist = useHistory();
  const { name, pickup_address, delivery_address, _id, status, created_date } =
    load;
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const [isLoad, setIsLoad] = useState(false)
  const fetcher = new FetchData();
  return (
    <>
      <section className="section lodsSection">
        <Cell title="LOAD NAME" value={name} />
        <Cell
          title="CREATED DATE"
          value={new Date(created_date).toLocaleString()}
        />
        <Cell title="PICK-UP ADDRESS" value={pickup_address} />
        <Cell title="DELIVERY ADDRESS" value={delivery_address} />
        <div className="cell cellLinks">
          <Link className="btn btnEdit" to={`/loads/${_id}/view`}>
            <Eye />
          </Link>
          {user.role === 'SHIPPER' && (
            <Link
              className={`btn btnEdit${
                status !== 'NEW' ? ' btnEditDisabled' : ''
              }`}
              to={`/loads/${_id}/edit`}
              onClick={(event) => {
                if (status !== 'NEW') {
                  event.preventDefault();
                }
              }}
            >
              <Edit />
            </Link>
          )}
          {user.role === 'SHIPPER' && (
            <Link
              className={`btn btnDelete${
                status !== 'NEW' ? ' btnDeleteDisabled' : ''
              }`}
              to={`/loads/${_id}/delete`}
              onClick={(event) => {
                event.preventDefault();
                if (status !== 'NEW') {
                  return;
                }
                setShouldUpdate(true);
                fetcher.processData({
                  jwt,
                  type: 'DELETE_LOAD',
                  id: _id,
                  setError,
                  setLoad: setIsLoad,
                  setData: setSuccess,
                });
              }}
            ></Link>
          )}
          {load.status === 'NEW' && user.role === 'SHIPPER' && (
            <button
              className="btn btnSubmit btyPost"
              type="button"
              onClick={() => {
                fetcher.processData({
                  jwt,
                  type: 'LOAD_SET_POST',
                  id: _id,
                  setError,
                  setData: setSuccess,
                  setLoad: setIsLoad,
                });
              }}
            >
              Post
            </button>
          )}
          {load.status === 'ASSIGNED' && user.role === 'DRIVER' && (
            <button
              className="btn btnSubmit btyPost"
              type="button"
              onClick={() => {
                try {
                  fetcher.processData({
                  jwt,
                  type: 'LOAD_SET_DRIVER_POST',
                  setError,
                  setData: setSuccess,
                  setLoad: setIsLoad
                  });
                  setShouldUpdate(true);
                } catch (error) {
                  setStatus('');
                  return <Redirect to='/loads/list' />;
                }
              }}
            >
              Update state
            </button>
          )}
        </div>
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {success && (
        <Modal
          message={success.message}
          setMessage={setSuccess}
          successAction={() => {
            setStatus('');
            hist.push('/loads/list');
          }
          }
        />
      )}
      {isLoad && <Preloader />}
    </>
  );
};

export default Load;
