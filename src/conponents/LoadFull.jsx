import React, { useContext, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import FetchData from '../helpers/dataFetcher';
import { AuthContext } from '../context/context';
import Modal from './Modal';
import Cell from './Cell';
import Preloader from './Preloader';

const LoadFull = () => {
  const { id } = useParams();
  const { jwt, user } = useContext(AuthContext);
  const [data, setData] = useState({});
  const [isLoad, setIsLoad] = useState(false);
  const [error, setError] = useState(null);
  const fetcher = new FetchData();
  const type = user.role === 'SHIPPER' ? 'GET_LOAD_SHIPPER' : 'GET_LOAD';

  useEffect(() => {
    fetcher.processData({
      jwt,
      setData,
      setLoad: setIsLoad,
      setError,
      type: type,
      id,
    });
  }, [error, id, jwt]);
  const { load, truck } = data;
  return (
    <>
      <section>
        {!isLoad && load && (
          <>
            <Cell title="LOAD NAME" value={load.name} />
            <Cell
              title="CREATED DATE"
              value={new Date(load.created_date).toLocaleString()}
            />
            <Cell title="PICK-UP ADDRESS" value={load.pickup_address} />
            <Cell title="DELIVERY ADDRESS" value={load.delivery_address} />
            <Cell title="STATUS" value={load.status} />
            <Cell title="STATE" value={load.state} />
            <Cell title="PAYLOAD" value={load.payload} />
            <Cell title="WIDTH" value={load.dimensions.width} />
            <Cell title="HEIGHT" value={load.dimensions.height} />
            <Cell title="LENGTH" value={load.dimensions.length} />
            {truck && (
              <>
                <Cell title="TRUCK TYPE" value={truck.type} />
                <Cell title="DRIVER ID" value={truck.assigned_to} />
                <Cell title="TRUCK STATUS" value={truck.status} />
              </>
            )}
            <div className="logs">
              <h2 className="logsTitle">LOGS</h2>
              {load.logs.length > 0 ? (
                load.logs.map((log, i) => (
                  <Cell
                    title={new Date(log.time).toLocaleString()}
                    value={log.message}
                    key={`${log.time}${i}`}
                  />
                ))
              ) : (
                <p className="cellText">No logs</p>
              )}
            </div>
          </>
        )}
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {isLoad && <Preloader />}
    </>
  );
};

export default LoadFull;
