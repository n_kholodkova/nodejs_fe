import React, { useContext, useState, useEffect } from 'react';
import Load from './Load';
import FetchData from '../helpers/dataFetcher';
import { AuthContext } from '../context/context';
import Modal from './Modal';
import Preloader from './Preloader';

const Loads = ({ status, setStatus }) => {
  const { jwt, user } = useContext(AuthContext);
  const [loads, setLoads] = useState(null);
  const [isLoad, setIsLoad] = useState(false);
  const [error, setError] = useState(null);
  const [shouldUpdate, setShouldUpdate] = useState(false);
  const fetcher = new FetchData();
  useEffect(() => {
    setShouldUpdate(false);
    fetcher.processData({
      jwt,
      setData: setLoads,
      setLoad: setIsLoad,
      setError,
      type: 'GET_LOADS',
      status,
    });
  }, [error, status, shouldUpdate]);
  return (
    <>
      {!isLoad && loads && (
        <>
          {loads.map((load) => (
            <Load load={load} key={load._id} jwt={jwt} user={user} setStatus={setStatus} setShouldUpdate={setShouldUpdate} />
          ))}
        </>
      )}
      {error && <Modal message={error} setMessage={setError} isError />}
      {isLoad && <Preloader />}
      {!isLoad && loads?.length === 0 && <section className="section">
        <h1 className="titleModal">No Loads found</h1>
      </section>}
    </>
  );
};

export default Loads;
