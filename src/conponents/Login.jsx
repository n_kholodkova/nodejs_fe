import React, { useContext, useState } from 'react';
import { AuthContext } from '../context/context';
import Modal from './Modal';
import Preloader from './Preloader';
import FetchData from '../helpers/dataFetcher';

const Login = () => {
  const { setJwt } = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [load, setLoad] = useState(false);
  const fetcher = new FetchData();
  const handler = async ({ target }) => {
    await fetcher.processData({
      type: 'SIGNIN',
      setError,
      setData: setJwt,
      setLoad,
      formData: Object.fromEntries([...new FormData(target)]),
    });
  };
  return (
    <>
      <section className="section">
        <form
          className="form"
          onSubmit={(event) => {
            event.preventDefault();
            handler(event);
          }}
        >
          <label className="label">
            <input
              className="input"
              type="email"
              placeholder="Email"
              onChange={({ target }) => {
                setEmail(target.value);
              }}
              value={email}
              name="email"
              required
            />
          </label>
          <label className="label">
            <input
              className="input"
              type="password"
              placeholder="Password"
              onChange={({ target }) => {
                setPassword(target.value);
              }}
              value={password}
              name="password"
              required
            />
          </label>
          <button type="submit" className="btn btnSubmit">
            Submit
          </button>
        </form>
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {load && <Preloader />}
    </>
  );
};

export default Login;
