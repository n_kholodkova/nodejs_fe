import React, { useState, useEffect } from 'react';

const Modal = ({ message, setMessage, isError, successAction }) => {
  const [visible, setVisible] = useState(true);
  useEffect(() => {
    const timer = setTimeout(() => {
      setVisible(false);
      setMessage(null);
      if (successAction) {
        successAction();
      }
    }, 3000);
    return () => clearTimeout(timer)
  });
  return (
    <div
      className={`modal ${visible ? 'modalVisible' : ''} ${
        isError ? 'modalError' : 'modalInfo'
      }`}
    >
      <h2 className="titleModal">{message}</h2>
    </div>
  );
};

export default Modal;
