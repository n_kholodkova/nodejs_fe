import React from 'react';
import spinner from '../assets/loader.svg';

const Preloader = () => (
  <section className="sectionLoader">
    <img className="loader" src={spinner} alt="loader" />
  </section>
);

export default Preloader;
