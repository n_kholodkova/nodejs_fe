import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../context/context';
import TableRow from './TableRow';
import Modal from './Modal';
import FetchData from '../helpers/dataFetcher';

const Profile = () => {
  const { user, jwt, setJwt, setUser } = useContext(AuthContext);
  const hist = useHistory();
  const [deleteSuccess, setDeleteSuccess] = useState(null);
  const [deleteError, setDeleteError] = useState(null);
  const fetcher = new FetchData();
  if (deleteSuccess) {
    setJwt(null);
    setUser(null);
  }
  return (
    <>
      <section className="section">
        {user && (
          <>
            <table className="table">
              <tbody>
                <TableRow title="Email" value={user.email} />
                <TableRow title="Role" value={user.role} />
                <TableRow
                  title="Profile created"
                  value={new Date(user.created_date).toLocaleString()}
                />
              </tbody>
            </table>
            <button
              type="button"
              className="btn btnSubmit btnChangePass"
              onClick={() => hist.push('/me/changePassword')}
            >
              Change Password
            </button>
            <button
              type="button"
              className="btn btnSubmit btnChangePass"
              onClick={() => {
                fetcher.processData({
                  jwt,
                  type: 'DELETE_USER',
                  setError: setDeleteError,
                  setData: setDeleteSuccess,
                });
              }}
            >
              Delete Profile
            </button>
          </>
        )}
        {!user && <h1 className="modileTitle">You need to login or signup</h1>}
      </section>
      {deleteError && (
        <Modal message={deleteError} setMessage={setDeleteError} isError />
      )}
      {deleteSuccess && (
        <Modal
          message={deleteSuccess}
          setMessage={setDeleteSuccess}
          successAction={() => hist.push('/signin')}
        />
      )}
    </>
  );
};

export default Profile;
