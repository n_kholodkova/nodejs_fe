import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import Modal from './Modal';
import Preloader from './Preloader';
import FetchData from '../helpers/dataFetcher';

const Signup = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState('');
  const [error, setError] = useState(null);
  const [load, setLoad] = useState(false);
  const [success, setSuccess] = useState(false);
  const fetcher = new FetchData();
  if (success && !load) {
    return <Redirect to='/signin' />;
  }
  const handler = async ({ target }) => {
    await fetcher.processData({
      type: 'SIGNUP',
      setError,
      setData: setSuccess,
      setLoad,
      formData: Object.fromEntries([...new FormData(target)]),
    });
  };
  return (
    <>
      <section className="section">
        <form
          className="form"
          onSubmit={(event) => {
            event.preventDefault();
            handler(event);
          }}
        >
          <label className="label">
            <input
              className="input"
              type="email"
              placeholder="Email"
              onChange={({ target }) => {
                setEmail(target.value);
              }}
              value={email}
              name="email"
              required
            />
          </label>
          <label className="label">
            <input
              className="input"
              type="password"
              placeholder="Password"
              onChange={({ target }) => {
                setPassword(target.value);
              }}
              value={password}
              name="password"
              required
            />
          </label>
          <label className="label">
            <input
              className="input"
              type="text"
              placeholder="Role"
              onChange={({ target }) => {
                setRole(target.value);
              }}
              value={role}
              name="role"
              required
            />
          </label>
          <button type="submit" className="btn btnSubmit">
            Submit
          </button>
        </form>
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {load && <Preloader />}
    </>
  );
};

export default Signup;
