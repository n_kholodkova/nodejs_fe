import React from 'react';

const TableHead = ({ items }) => (
  <thead>
    <tr className="tr">
      {items.map((item, i) => (
        <th className="th" key={`${item}${i}`}>
          {item}
        </th>
      ))}
    </tr>
  </thead>
);

export default TableHead;
