import React from 'react';

const TableRow = ({ title, value }) => (
  <tr className="tr">
    <th className="th">{title}</th>
    <td className="td">{value}</td>
  </tr>
);

export default TableRow;
