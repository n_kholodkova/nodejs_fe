import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Eye from './Eye';
import Edit from './Edit';
import Cell from './Cell';
import FetchData from '../helpers/dataFetcher';
import Modal from './Modal';
import Preloader from './Preloader';

const Truck = ({ truck, jwt, setShouldUpdate }) => {
  const { type, _id, status, created_date } = truck;
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const [load, setLoad] = useState(false);
  const fetcher = new FetchData();
  return (
    <>
      <section className="section lodsSection">
        <Cell title="TRUCK TYPE" value={type} />
        <Cell
          title="CREATED DATE"
          value={new Date(created_date).toLocaleString()}
        />
        <Cell title="STATUS" value={status} />
        <div className="cell cellLinks">
          <Link className="btn btnEdit" to={`/trucks/${_id}/view`}>
            <Eye />
          </Link>
          <Link
            className={`btn btnEdit${
              status !== 'IS' ? ' btnEditDisabled' : ''
            }`}
            to={`/trucks/${_id}/edit`}
            onClick={(event) => {
              if (status === 'OS') {
                event.preventDefault();
                return;
              }
            }}
          >
            <Edit />
          </Link>
          {!truck.assigned_to && (<>
            <Link
            className={`btn btnDelete${
              status !== 'IS' ? ' btnDeleteDisabled' : ''
            }`}
            to={`/trucks/${_id}/delete`}
            onClick={(event) => {
              event.preventDefault();
              if (status !== 'IS') {
                return;
              }
              setShouldUpdate(true);
              fetcher.processData({
                jwt,
                type: 'DELETE_TRUCK',
                id: _id,
                setError,
                setData: setSuccess,
                setLoad
              });
            }}
          ></Link>
            <button
              className="btn btnSubmit btyPost"
              type="button"
              onClick={() => {
                setShouldUpdate(true);
                fetcher.processData({
                  jwt,
                  type: 'ASSIGN_TRUCK',
                  id: _id,
                  setError,
                  setData: setSuccess,
                  setLoad
                });
              }}
            >
              Assign
            </button>
          </>)}
        </div>
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {success && (
        <Modal
          message={success.message}
          setMessage={setSuccess}
        />
      )}
      {load && <Preloader />}
    </>
  );
};

export default Truck;
