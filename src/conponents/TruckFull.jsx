import React, { useContext, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import FetchData from '../helpers/dataFetcher';
import { AuthContext } from '../context/context';
import Modal from './Modal';
import Cell from './Cell';
import Preloader from './Preloader';

const TruckFull = () => {
  const { id } = useParams();
  const { jwt } = useContext(AuthContext);
  const [truck, setTruck] = useState({});
  const [isLoad, setIsLoad] = useState(false);
  const [error, setError] = useState(null);
  const fetcher = new FetchData();
  useEffect(() => {
    fetcher.processData({
      jwt,
      setData: setTruck,
      setLoad: setIsLoad,
      setError,
      type: 'GET_TRUCK',
      id,
    });
  }, [error, id, jwt]);
  return (
    <>
      <section className="section">
        {!isLoad && truck && (
          <>
            <Cell title="TRUCK TYPE" value={truck.type} />
            <Cell
              title="CREATED DATE"
              value={new Date(truck.created_date).toLocaleString()}
            />
            <Cell title="STATUS" value={truck.status} />
            <Cell title="ASSIGNED TO" value={truck.assigned_to || '-'} />
          </>
        )}
      </section>
      {error && <Modal message={error} setMessage={setError} isError />}
      {isLoad && <Preloader />}
    </>
  );
};

export default TruckFull;
