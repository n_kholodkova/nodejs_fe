import React, { useContext, useState, useEffect } from 'react';
import Truck from './Truck';
import FetchData from '../helpers/dataFetcher';
import { AuthContext } from '../context/context';
import Modal from './Modal';
import Preloader from './Preloader';

const Trucks = ({ status }) => {
  const { jwt, user } = useContext(AuthContext);
  const [trucks, setTrucks] = useState(null);
  const [isLoad, setIsLoad] = useState(false);
  const [error, setError] = useState(null);
  const [shouldUpdate, setShouldUpdate] = useState(false);
  const fetcher = new FetchData();
  useEffect(() => {
    setShouldUpdate(false);
    fetcher.processData({
      jwt,
      setData: setTrucks,
      setLoad: setIsLoad,
      setError,
      type: 'GET_TRUCKS',
      status,
    });
  }, [error, status, shouldUpdate]);
  return (
    <>
      {!isLoad && trucks && (
        <>
          {trucks.map((truck) => (
            <Truck truck={truck} key={truck._id} jwt={jwt} user={user} setShouldUpdate={setShouldUpdate} />
          ))}
        </>
      )}
      {error && <Modal message={error} setMessage={setError} isError />}
      {isLoad && <Preloader />}
      {!isLoad && trucks?.length === 0 && <section className="section">
        <h1 className="titleModal">No Loads found</h1>
      </section>}
    </>
  );
};

export default Trucks;
