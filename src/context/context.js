import React, { useState, createContext, useEffect } from 'react';
import FetchData from '../helpers/dataFetcher';
const AuthContext = createContext();
const AuthContextProvider = ({ children }) => {
  const fetcher = new FetchData();
  const [jwt, setJwt] = useState('');
  const [user, setUser] = useState(null);
  const [userError, setUserError] = useState(null);
  const [userLoad, setUserLoad] = useState(false);
  useEffect(() => {
    if (jwt) {
      fetcher.processData({
        jwt,
        setData: setUser,
        type: 'GET_PROFILE',
        setError: setUserError,
        setLoad: setUserLoad,
      });
    }
  }, [jwt]);
  return (
    <AuthContext.Provider
      value={{ jwt, setJwt, user, setUser, userError, userLoad }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthContextProvider };
