class FetchData {
  constructor() {
    this.url = 'https://load-system.herokuapp.com';
  }

  getHeader = (jwt) => ({
    'Content-type': 'application/json',
    Authorization: `Bearer ${jwt}`,
  });

  signIn = async (formData) =>
    fetch(`${this.url}/api/auth/login`, {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: { 'Content-type': 'application/json' },
    });

  signUp = async (formData) =>
    fetch(`${this.url}/api/auth/register`, {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: { 'Content-type': 'application/json' },
    });

  changePassword = async (jwt, formData) =>
    fetch(`${this.url}/api/users/me/password`, {
      method: 'PATCH',
      body: JSON.stringify(formData),
      headers: this.getHeader(jwt),
    });

  getUserProfile = async (jwt) =>
    fetch(`${this.url}/api/users/me`, {
      method: 'get',
      headers: this.getHeader(jwt),
    });

  getLoads = async (jwt, status) =>
    fetch(`${this.url}/api/loads${status ? `?status=${status}` : ''}`, {
      method: 'get',
      headers: this.getHeader(jwt),
    });

  getLoadsActive = async (jwt) =>
    fetch(`${this.url}/api/loads/active`, {
      method: 'get',
      headers: this.getHeader(jwt),
    });

  getLoad = async (jwt, id) =>
    fetch(`${this.url}/api/loads/${id}`, {
      method: 'get',
      headers: this.getHeader(jwt),
    });

  getLoadSipper = async (jwt, id) =>
  fetch(`${this.url}/api/loads/${id}/shipping_info`, {
    method: 'get',
    headers: this.getHeader(jwt),
  });

  loadChangeStatusPost = async (jwt, id) =>
    fetch(`${this.url}/api/loads/${id}/post`, {
      method: 'POST',
      headers: this.getHeader(jwt),
    });

  loadChangeStatusDriver = async (jwt) =>
    fetch(`${this.url}/api/loads/active/state`, {
      method: 'PATCH',
      headers: this.getHeader(jwt),
    });

  updateLoad = async (jwt, id, formData) =>
    fetch(`${this.url}/api/loads/${id}`, {
      method: 'PUT',
      headers: this.getHeader(jwt),
      body: JSON.stringify(formData),
    });

  postLoad = async (jwt, formData) =>
    fetch(`${this.url}/api/loads`, {
      method: 'POST',
      headers: this.getHeader(jwt),
      body: JSON.stringify(formData),
    });

  getTrucks = async (jwt) =>
    fetch(`${this.url}/api/trucks`, {
      method: 'get',
      headers: this.getHeader(jwt),
    });

  getTruck = async (jwt, id) =>
    fetch(`${this.url}/api/trucks/${id}`, {
      method: 'get',
      headers: this.getHeader(jwt),
    });

  assignTruck = (jwt, id) =>
    fetch(`${this.url}/api/trucks/${id}/assign`, {
      method: 'POST',
      headers: this.getHeader(jwt),
    });

  updateTruck = async (jwt, id, formData) =>
    fetch(`${this.url}/api/trucks/${id}`, {
      method: 'PUT',
      headers: this.getHeader(jwt),
      body: JSON.stringify(formData),
    });

  postTruck = async (jwt, formData) =>
    fetch(`${this.url}/api/trucks`, {
      method: 'POST',
      headers: this.getHeader(jwt),
      body: JSON.stringify(formData),
    });

  deleteItem = async (jwt, id, key, setError, setData) => {
    try {
      const response = await fetch(`${this.url}/api/${key}/${id}`, {
        method: 'DELETE',
        headers: this.getHeader(jwt),
      });
      const data = await response.json();
      if (response.status !== 200) {
        setError(data.message);
      } else {
        setData(data);
      }
    } catch (error) {
      setError(error.message);
    }
  };

  runSetters = async (response, setData, setLoad, setError, key) => {
    try {
      const data = await response.json();
      if (response.status !== 200) {
        setError(data.message);
      } else {
        if (setData && key) {
          setData(data[key]);
        } else {
          if (setData) {
            setData(data);
          }
        }
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoad(false);
    }
  };

  processData = async ({
    jwt,
    setData,
    setLoad,
    setError,
    type,
    status,
    id,
    formData,
  }) => {
    if (setLoad) {
      setLoad(true);
    }
    let response;
    let key;
    switch (type) {
      case 'CHANGE_PASSWORD':
        response = await this.changePassword(jwt, formData);
        break;
      case 'SIGNIN':
        response = await this.signIn(formData);
        key = 'jwt_token';
        break;
      case 'SIGNUP':
        response = await this.signUp(formData);
        break;
      case 'GET_PROFILE':
        response = await this.getUserProfile(jwt);
        key = 'user';
        break;
      case 'DELETE_USER':
        await this.deleteItem(jwt, 'me', 'users', setError, setData);
        return;
      case 'LOAD_SET_POST':
        response = await this.loadChangeStatusPost(jwt, id);
        break;
      case 'LOAD_SET_DRIVER_POST':
        response = await this.loadChangeStatusDriver(jwt);
        break;
      case 'ADD_LOAD':
        response = await this.postLoad(jwt, formData);
        break;
      case 'UPDATE_LOAD':
        response = await this.updateLoad(jwt, id, formData);
        break;
      case 'GET_LOADS':
        response = await this.getLoads(jwt, status);
        key = 'loads';
        break;
      case 'GET_LOAD':
        response = await this.getLoad(jwt, id);
        break;
      case 'GET_LOAD_SHIPPER':
        response = await this.getLoadSipper(jwt, id);
        break;
      case 'GET_LOADS_ACTIVE':
        response = await this.getLoadsActive(jwt);
        key = 'loads';
        break;
      case 'DELETE_LOAD':
        await this.deleteItem(jwt, id, 'loads', setError, setData);
        break;
      case 'ADD_TRUCK':
        response = await this.postTruck(jwt, formData);
        break;
      case 'UPDATE_TRUCK':
        response = await this.updateTruck(jwt, id, formData);
        break;
      case 'GET_TRUCK':
        response = await this.getTruck(jwt, id);
        key = 'truck';
        break;
      case 'GET_TRUCKS':
        response = await this.getTrucks(jwt);
        key = 'trucks';
        break;
      case 'ASSIGN_TRUCK':
        response = await this.assignTruck(jwt, id);
        return;
      case 'DELETE_TRUCK':
        await this.deleteItem(jwt, id, 'trucks', setError, setData);
        break;
      default:
        setLoad(false);
        return;
    }
    await this.runSetters(response, setData, setLoad, setError, key);
  };
}

export default FetchData;
